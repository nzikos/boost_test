#/bin/bash!
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build . --target UnitTest
./test/UnitTest
cd ..
#mkdir coverage
#gcovr -r .. -e '.*deterministicRandom.h' -e '.*test/.*' --html --html-details -s -o ./coverage/coverage.html